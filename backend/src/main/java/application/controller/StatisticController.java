package application.controller;

import application.service.StatisticService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.YearMonth;

@RestController
@RequestMapping("/statistic")
@Slf4j
public class StatisticController {

    @Autowired
    private StatisticService statisticService;

    @GetMapping(params = {"year", "month"})
    public ResponseEntity getStatisticByYearMonth (@RequestParam int year, @RequestParam int month) {
        YearMonth yearMonth = YearMonth.of(year, month);
        log.debug(String.format("Get statistic by %s.", yearMonth));
        return new ResponseEntity<>(statisticService.getStatisticByYearMonth(yearMonth), HttpStatus.OK);
    }

}
