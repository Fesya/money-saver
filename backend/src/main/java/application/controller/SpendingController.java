package application.controller;

import application.entity.Spending;
import application.service.SpendingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.YearMonth;
import java.util.List;

@RestController
@RequestMapping("/spending")
public class SpendingController {

    private final SpendingService spendingService;

    @Autowired
    public SpendingController (SpendingService spendingService) {
        this.spendingService = spendingService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<Spending> getSpendingById (@PathVariable("id") long id) {
        return spendingService
                .findById(id)
                .map(spending -> new ResponseEntity<>(spending, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @GetMapping
    @RequestMapping(path = "/range", params = {"year", "month"})
    public ResponseEntity<List<Spending>> getSpendingsForYearMonth (@RequestParam("year") int year,
                                                                    @RequestParam("month") int month) {
        return new ResponseEntity<>(spendingService.getSpendingByMonth(YearMonth.of(year, month)).getSpendings(), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Spending> createSpending (@RequestBody Spending spending) {
        return new ResponseEntity<>(spendingService.save(spending), HttpStatus.CREATED);
    }

    @DeleteMapping(params = "id")
    public ResponseEntity deleteSpending (@RequestParam("id") long id) {
        spendingService.deleteById(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

}
