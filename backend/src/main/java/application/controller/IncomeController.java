package application.controller;

import application.entity.Income;
import application.service.IncomeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.YearMonth;

@RestController
@RequestMapping("/income")
public class IncomeController {
    @Autowired
    private IncomeService incomeService;

    @GetMapping(params = {"year", "month"})
    public ResponseEntity getIncome (@RequestParam("year") int year, @RequestParam("month") int month) {
        YearMonth yearMonth = YearMonth.of(year, month);
        return new ResponseEntity<>(incomeService.getIncomeByMonth(yearMonth).getIncomes(), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity saveIncome (@RequestBody Income income) {
        return new ResponseEntity<>(incomeService.save(income).getId(), HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity updateIncome (@RequestBody Income income) {
        return new ResponseEntity<>(incomeService.save(income), HttpStatus.ACCEPTED);
    }
}
