package application.controller;

import application.dto.SavingDTO;
import application.entity.MoneyError;
import application.entity.Saving;
import application.service.SavingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.YearMonth;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController()
@RequestMapping("/saving")
public class SavingController {

    @Autowired
    private SavingService savingService;

    @GetMapping()
    public ResponseEntity getSaving(@RequestParam int year, @RequestParam int month) {
        Optional<Saving> saving = savingService.getSaving(YearMonth.of(year, month));
        return saving.map(value -> new ResponseEntity<>(value, HttpStatus.OK)).orElseGet(() -> new ResponseEntity(HttpStatus.NOT_FOUND));
    }

    @RequestMapping(method = {RequestMethod.POST, RequestMethod.PUT})
    public ResponseEntity saveSaving(@RequestBody @Valid SavingDTO saving, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            List<String> errors = bindingResult
                    .getAllErrors()
                    .stream()
                    .map(DefaultMessageSourceResolvable::getDefaultMessage)
                    .collect(Collectors.toList());
            MoneyError moneyError = new MoneyError()
                    .setError(errors);
            return new ResponseEntity<>(moneyError, HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(savingService.save(Saving.mapDTO(saving)), HttpStatus.CREATED);
    }
}
