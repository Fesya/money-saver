package application.repository;

import application.entity.Income;
import org.springframework.data.repository.CrudRepository;

import java.time.LocalDate;
import java.util.List;


public interface IncomeRepository extends CrudRepository<Income, Long> {

    List<Income> findAllByCreationDateBetween (LocalDate start, LocalDate end);


}
