package application.repository;

import application.entity.Saving;
import org.springframework.data.repository.CrudRepository;

import java.time.YearMonth;

public interface SavingRepository extends CrudRepository<Saving, YearMonth> {
}
