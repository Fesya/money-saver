package application.repository;

import application.entity.Spending;
import org.springframework.data.repository.CrudRepository;

import java.time.LocalDate;
import java.util.List;

public interface SpendingRepository extends CrudRepository<Spending, Long> {

    List<Spending> findAll ();

    List<Spending> findAllByCreationDateBetween (LocalDate startDate, LocalDate endDate);
}
