package application.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.YearMonth;

@Data
@Accessors(chain = true)
public class SavingDTO {

    @NotNull(message = "Date is required.")
    private YearMonth yearMonth;

    @Min(value = 0, message = "Percent can not be less than 0.")
    @Max(value = 100, message = "Percent can not be greater than 100.")
    private BigDecimal percent;
}
