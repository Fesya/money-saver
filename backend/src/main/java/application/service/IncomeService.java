package application.service;

import application.entity.Income;
import application.entity.MonthlyIncome;
import application.repository.IncomeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.YearMonth;
import java.util.List;


@Service
public class IncomeService {
    private final IncomeRepository incomeRepository;

    @Autowired
    public IncomeService (IncomeRepository incomeRepository) {
        this.incomeRepository = incomeRepository;
    }

    public Income save (Income income) {
        return incomeRepository.save(income);
    }

    public MonthlyIncome getIncomeByMonth (YearMonth yearMonth) {
        LocalDate endOfMonth = yearMonth.atEndOfMonth();
        List<Income> incomes = incomeRepository.findAllByCreationDateBetween(yearMonth.atDay(1), endOfMonth);
        return new MonthlyIncome(incomes);
    }

}
