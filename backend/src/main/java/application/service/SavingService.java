package application.service;

import application.entity.Saving;
import application.repository.SavingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.YearMonth;
import java.util.Optional;

@Service
public class SavingService {

    private final SavingRepository savingRepository;

    @Autowired
    public SavingService (SavingRepository savingRepository) {
        this.savingRepository = savingRepository;
    }

    public Saving save (Saving saving) {
        return savingRepository.save(saving);
    }

    public Optional<Saving> getSaving (YearMonth yearMonth) {
        return savingRepository.findById(yearMonth);
    }
}
