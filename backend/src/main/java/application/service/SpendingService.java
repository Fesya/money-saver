package application.service;

import application.entity.MonthlySpending;
import application.entity.Spending;
import application.repository.SpendingRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.YearMonth;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class SpendingService {

    private final SpendingRepository spendingRepository;

    public Spending save (Spending spending) {
        return spendingRepository.save(spending);
    }

    public void deleteById (long id) {
        spendingRepository.deleteById(id);
    }

    public Optional<Spending> findById (long id) {
        return spendingRepository.findById(id);
    }

    public MonthlySpending getSpendingByMonth (YearMonth yearMonth) {
        List<Spending> spendings = spendingRepository.findAllByCreationDateBetween(yearMonth.atDay(1), yearMonth.atEndOfMonth());
        return new MonthlySpending(spendings);
    }
}
