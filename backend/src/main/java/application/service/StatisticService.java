package application.service;

import application.entity.MonthlyIncome;
import application.entity.MonthlySpending;
import application.entity.MonthlyStatistic;
import application.entity.Saving;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Month;
import java.time.YearMonth;

@Slf4j
@Service
public class StatisticService {

    private final IncomeService incomeService;

    private final SavingService savingService;

    private final SpendingService spendingService;

    @Autowired
    public StatisticService (IncomeService incomeService, SavingService savingService, SpendingService spendingService) {
        this.incomeService = incomeService;
        this.savingService = savingService;
        this.spendingService = spendingService;
    }

    public MonthlyStatistic getStatisticByYearMonth (YearMonth yearMonth) {
        MonthlyIncome monthlyIncome = incomeService.getIncomeByMonth(yearMonth);
        MonthlySpending monthlySpending = spendingService.getSpendingByMonth(yearMonth);

        Saving savingPercent = savingService
                .getSaving(yearMonth)
                .orElse(Saving.ZERO_SAVING);

        BigDecimal savingAmountMonthly = calculateSavingByMonth(monthlyIncome, savingPercent);
        BigDecimal savingAmountYear = calculateSavingByYear(monthlyIncome, savingPercent);
        BigDecimal amountPerDay = calculateAmountPerDay(yearMonth, monthlyIncome, monthlySpending, savingAmountMonthly);

        return new MonthlyStatistic()
                .setMonthlyIncome(monthlyIncome)
                .setSaving(savingPercent.getPercent())
                .setSavingsByMonth(savingAmountMonthly)
                .setSavingByYear(savingAmountYear)
                .setAmountPerDay(amountPerDay);

    }

    private BigDecimal calculateAmountPerDay (YearMonth yearMonth, MonthlyIncome monthlyIncome, MonthlySpending monthlySpending, BigDecimal savingAmount) {
        return monthlyIncome
                .getMonthlyIncome()
                .subtract(savingAmount)
                .subtract(monthlySpending.getMonthlySpending())
                .divide(BigDecimal.valueOf(yearMonth.lengthOfMonth()), 2, RoundingMode.DOWN);
    }

    private BigDecimal calculateSavingByYear (MonthlyIncome monthlyIncome, Saving percent) {
        return calculateSavingByMonth(monthlyIncome, percent)
                .multiply(BigDecimal.valueOf(12));
    }

    private BigDecimal calculateSavingByMonth (MonthlyIncome monthlyIncome, Saving percent) {
        return monthlyIncome
                .getMonthlyIncome()
                .multiply(percent.getPercent())
                .divide(BigDecimal.valueOf(100), 2, RoundingMode.HALF_DOWN);
    }
}
