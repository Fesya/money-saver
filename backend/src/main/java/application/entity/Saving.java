package application.entity;

import application.dto.SavingDTO;
import lombok.*;
import lombok.experimental.Accessors;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.YearMonth;

@Setter
@Getter
@Entity
@ToString
@NoArgsConstructor
@EqualsAndHashCode
@Accessors(chain = true)
public class Saving {

    public static final Saving ZERO_SAVING = new Saving().setPercent(BigDecimal.ZERO);

    @Id
    private YearMonth yearMonth;

    @NotNull
    private BigDecimal percent;

    public static Saving mapDTO(SavingDTO savingDTO) {
        return new Saving()
                .setPercent(savingDTO.getPercent())
                .setYearMonth(savingDTO.getYearMonth());
    }
}
