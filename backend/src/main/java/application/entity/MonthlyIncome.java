package application.entity;

import lombok.*;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;


@Getter
@Setter
@ToString
@NoArgsConstructor
@EqualsAndHashCode
@Accessors(chain = true)
public class MonthlyIncome {

    @Singular
    List<Income> incomes;
    private BigDecimal monthlyIncome;

    public MonthlyIncome (List<Income> incomes) {
        this.incomes = incomes;
        this.monthlyIncome = incomes.stream()
                .map(Income::getSum)
                .reduce(BigDecimal.ZERO, BigDecimal::add)
                .setScale(2, RoundingMode.DOWN);
    }
}
