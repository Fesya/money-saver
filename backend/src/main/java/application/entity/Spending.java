package application.entity;

import lombok.*;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
@Entity
@ToString
@NoArgsConstructor
@EqualsAndHashCode
@Accessors(chain = true)
public class Spending {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false)
    private LocalDate creationDate;

    private String description;

    @Column(nullable = false)
    private BigDecimal sum;

    @PrePersist
    public void setDefaultDate () {
        if (this.creationDate == null) {
            this.creationDate = LocalDate.now();
        }
    }
}
