package application.entity;

import lombok.*;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
@EqualsAndHashCode
@Accessors(chain = true)
public class MonthlySpending {

    @Singular
    List<Spending> spendings;
    private BigDecimal monthlySpending;

    public MonthlySpending (List<Spending> spendings) {
        this.spendings = spendings;
        this.monthlySpending = spendings.stream()
                .map(Spending::getSum)
                .reduce(BigDecimal.ZERO, BigDecimal::add)
                .setScale(2, RoundingMode.DOWN);
    }

}
