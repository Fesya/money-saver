package application.entity;


import lombok.*;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

@Getter
@Setter
@ToString
@NoArgsConstructor
@EqualsAndHashCode
@Accessors(chain = true)
public class MonthlyStatistic {

    private MonthlyIncome monthlyIncome;
    private BigDecimal saving;
    private BigDecimal amountPerDay;
    private BigDecimal savingsByMonth;
    private BigDecimal savingByYear;
}
