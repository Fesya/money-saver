package application.controller;

import application.Application;
import com.github.javafaker.Faker;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Locale;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@RunWith(SpringJUnit4ClassRunner.class)
@AutoConfigureTestDatabase
@SpringBootTest(webEnvironment = RANDOM_PORT, classes = Application.class)
public abstract class BaseIntegration {

    //This variable was implemented as fake test-data generator
    final Faker faker = new Faker(new Locale("en-US"));
    String TEST_DESCRIPTION;

    @Before
    public void initBase () {
        TEST_DESCRIPTION = faker.rickAndMorty().character();
    }
}
