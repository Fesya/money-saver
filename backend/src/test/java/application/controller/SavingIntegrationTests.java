package application.controller;

import application.entity.MoneyError;
import application.entity.Saving;
import application.repository.SavingRepository;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.YearMonth;
import java.util.Optional;
import java.util.Random;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class SavingIntegrationTests extends BaseIntegration {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private SavingRepository savingRepository;

    private Saving saving;
    private int random;

    @Before
    public void setUp () {
        random = new Random().nextInt(101);
        saving = new Saving()
                .setPercent(new BigDecimal(random))
                .setYearMonth(YearMonth.now());

        savingRepository.save(saving);
    }

    @Test
    public void GetSavingByYearMonth () {
        YearMonth yearMonth = YearMonth.now()
                .minusYears(5)
                .minusMonths(5);

        Saving shouldBeFound = new Saving()
                .setPercent(new BigDecimal(10.4).setScale(2, RoundingMode.HALF_UP))
                .setYearMonth(yearMonth);
        savingRepository.save(shouldBeFound);

        ResponseEntity<Saving> response = testRestTemplate.getForEntity("/saving?year={year}&month={month}", Saving.class, yearMonth.getYear(), yearMonth.getMonthValue());
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));

        Saving body = response.getBody();
        assertThat(body, notNullValue());
        assertThat(body, equalTo(shouldBeFound));
        assertThat(body, not(equalTo(saving)));
    }

    @Test
    public void NotExistingSavingShouldGetAnError () {
        ResponseEntity<Saving> response = testRestTemplate.getForEntity("/saving?year={year}&month={month}", Saving.class, 153, 12);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.NOT_FOUND));
        assertThat(response.getBody(), nullValue());
    }

    @Test
    public void testSaveSaving () {
        Saving saving = new Saving()
                .setYearMonth(YearMonth.now().minusYears(5))
                .setPercent(new BigDecimal(random));

        ResponseEntity<Saving> response = testRestTemplate.postForEntity("/saving", saving, Saving.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.CREATED));

        Saving body = response.getBody();
        assertThat(body, notNullValue());
        assertThat(body, equalTo(saving));
    }

    @Test
    public void testUpdateSaving () {
        saving
                .setPercent(new BigDecimal(66).setScale(2))
                .setYearMonth(YearMonth.of(1945, 9));

        testRestTemplate.put("/saving", saving);

        Optional<Saving> savingFromDB = savingRepository.findById(this.saving.getYearMonth());
        assertThat(savingFromDB.isPresent(), is(true));
        assertThat(savingFromDB.get(), equalTo(saving));
    }


    @Test
    public void PercentShoulNotBeLessThenZeroOnUpdate () {
        saving.setPercent(new BigDecimal(-1));
        testValidationSingleField(saving, HttpMethod.PUT, "Percent can not be less than 0.");
    }

    @Test
    public void PercentShoulNotBeLessThenZeroOnCreate () {
        saving.setYearMonth(YearMonth.now().minusYears(5))
                .setPercent(new BigDecimal(-1));
        testValidationSingleField(saving, HttpMethod.POST, "Percent can not be less than 0.");
    }

    @Test
    public void PercentShouldNotBeBigger100OnUpdate () {
        saving.setPercent(new BigDecimal(101));
        testValidationSingleField(saving, HttpMethod.PUT, "Percent can not be greater than 100.");
    }

    @Test
    public void PercentShouldNotBeBigger100OnCreate () {
        saving.setYearMonth(YearMonth.now().minusYears(5))
                .setPercent(new BigDecimal(101));
        testValidationSingleField(saving, HttpMethod.POST, "Percent can not be greater than 100.");
    }

    @Test
    public void SavingDateShouldNotBeNull () {
        saving.setYearMonth(null);
        testValidationSingleField(saving, HttpMethod.POST, "Date is required.");
    }

    @Test
    public void MultipleMistakesShouldBeHandeledProperly () {
        saving.setYearMonth(null)
                .setPercent(new BigDecimal(-1));
        ResponseEntity<MoneyError> error = testRestTemplate.postForEntity("/saving", saving, MoneyError.class);

        assertThat(error.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));

        MoneyError body = error.getBody();
        assertThat(body, notNullValue());

        String[] expected = {"Date is required.", "Percent can not be less than 0."};
        assertThat(body.getError(), containsInAnyOrder(expected));
    }

    private void testValidationSingleField(Saving saving, HttpMethod httpMethod, String errorMessage) {
        HttpEntity<Saving> httpEntity = new HttpEntity<>(saving);

        ResponseEntity<MoneyError> exchange = testRestTemplate.exchange("/saving", httpMethod, httpEntity, MoneyError.class);
        assertThat(exchange.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));

        MoneyError body = exchange.getBody();
        assertThat(body, notNullValue());
        assertThat(body.getError(), hasItem(errorMessage));
    }
}
