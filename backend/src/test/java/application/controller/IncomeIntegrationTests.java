package application.controller;

import application.entity.Income;
import application.repository.IncomeRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.MonthDay;
import java.time.YearMonth;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class IncomeIntegrationTests extends BaseIntegration {

    @Autowired
    private TestRestTemplate incomeController;

    @Autowired
    private IncomeRepository incomeRepository;
    private Income income;

    @Before
    public void setUp () {
        LocalDate now = LocalDate.now();
        income = new Income()
                .setCreationDate(now)
                .setDescription(TEST_DESCRIPTION)
                .setSum(new BigDecimal(54.25).setScale(2, RoundingMode.HALF_UP));
        incomeRepository.save(income);
    }

    @After
    public void tearDown () {
        incomeRepository.deleteAll();
    }


    @Test
    public void IncomeShouldBeSavedProperly () {
        LocalDate now = LocalDate.now();
        Income income = new Income()
                .setCreationDate(now)
                .setDescription(TEST_DESCRIPTION)
                .setSum(new BigDecimal(89.23));
        ResponseEntity<Long> entity = incomeController.postForEntity("/income", income, Long.class);

        assertThat(entity.getStatusCode(), equalTo(HttpStatus.OK));
        assertThat(entity.getBody(), notNullValue(Long.class));
        assertThat(incomeRepository.existsById(entity.getBody()), is(Boolean.TRUE));
    }

    @Test
    public void IncomeShouldBeUpdatedProperly () {

        income.setDescription(faker.gameOfThrones().quote());
        income.setSum(new BigDecimal(678).setScale(2));

        incomeController.put("/income", income, Income.class);

        Optional<Income> updatedIncome = incomeRepository.findById(income.getId());

        assertThat("Optional should contain income.", updatedIncome.isPresent(), is(Boolean.TRUE));
        assertThat(updatedIncome.get(), equalTo(income));
    }

    @Test
    public void IncomeShouldBeFilteredByMonth () {
        YearMonth yearMonth = YearMonth
                .now()
                .minusMonths(1);

        LocalDate localDate = yearMonth
                .atDay(MonthDay.now().getDayOfMonth());

        Income incomeShouldBePresent = new Income()
                .setCreationDate(localDate)
                .setDescription(TEST_DESCRIPTION)
                .setSum(new BigDecimal(340.99).setScale(2, RoundingMode.HALF_UP));

        incomeRepository.save(incomeShouldBePresent);


        ResponseEntity<List<Income>> incomes = incomeController
                .exchange(
                        "/income/?month={month}&year={year}",
                        HttpMethod.GET,
                        null,
                        new ParameterizedTypeReference<List<Income>>() {
                        },
                        yearMonth.getMonthValue(),
                        yearMonth.getYear());

        assertThat(incomes.getStatusCode(), equalTo(HttpStatus.OK));

        List<Income> body = incomes.getBody();
        assertThat(body, notNullValue());
        assertThat("Collection should contain expected income", body, hasItem(incomeShouldBePresent));
        assertThat(body, not(hasItem(income)));
    }

}
