package application.controller;

import application.entity.*;
import application.repository.IncomeRepository;
import application.repository.SavingRepository;
import application.repository.SpendingRepository;
import org.apache.commons.lang3.RandomUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.data.repository.CrudRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.YearMonth;
import java.util.List;
import java.util.Random;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.core.IsNull.notNullValue;

public class StatisticIntegrationTest extends BaseIntegration {

    private final YearMonth now = YearMonth.now();
    @Autowired
    private TestRestTemplate testRestTemplate;
    @Autowired
    private IncomeRepository incomeRepository;
    @Autowired
    private SavingRepository savingRepository;
    @Autowired
    private SpendingRepository spendingRepository;
    private Saving saving;
    private MonthlyIncome monthlyIncome;
    private MonthlySpending monthlySpending;

    @Before
    public void setUp () {
        Supplier<LocalDate> randomMonthDay = () -> now.atDay(RandomUtils.nextInt(1, now.lengthOfMonth()));

        monthlyIncome = new MonthlyIncome(
                generate(
                        value -> new Income()
                                .setDescription(faker.pokemon().name())
                                .setCreationDate(randomMonthDay.get())
                                .setSum(value),
                        incomeRepository
                )
        );

        monthlySpending = new MonthlySpending(
                generate(
                        value -> new Spending()
                                .setDescription(faker.artist().name())
                                .setCreationDate(randomMonthDay.get())
                                .setSum(value),
                        spendingRepository
                )
        );

        saving = new Saving()
                .setYearMonth(now)
                .setPercent(
                        new BigDecimal(RandomUtils.nextInt(1, 100))
                                .setScale(2, RoundingMode.HALF_UP)
                );

        savingRepository.save(saving);
    }

    @After
    public void tearDown () {
        incomeRepository.deleteAll();
        spendingRepository.deleteAll();
        savingRepository.deleteAll();
    }

    private <R> List<R> generate (Function<BigDecimal, R> maper, CrudRepository<R, Long> repository) {
        List<R> list = new Random()
                .ints(RandomUtils.nextInt(5, 30), 1, 50000)
                .mapToObj(value -> BigDecimal.valueOf(value, 2))
                .map(maper)
                .collect(Collectors.toList());
        repository.saveAll(list);
        return list;
    }

    @Test
    public void testSavingByMonth () {
        ResponseEntity<MonthlyStatistic> entity = testRestTemplate.getForEntity("/statistic?year={year}&month={month}", MonthlyStatistic.class, now.getYear(), now.getMonthValue());
        assertThat(entity.getStatusCode(), equalTo(HttpStatus.OK));

        BigDecimal expectedSaving = getMonthlySaving(monthlyIncome);

        MonthlyStatistic monthlyStatistic = entity.getBody();
        assertThat(monthlyStatistic, notNullValue());
        assertThat(monthlyStatistic.getSavingsByMonth(), equalTo(expectedSaving));
    }

    @Test
    public void testSavingByYear () {
        ResponseEntity<MonthlyStatistic> entity = testRestTemplate.getForEntity("/statistic?year={year}&month={month}", MonthlyStatistic.class, now.getYear(), now.getMonthValue());
        assertThat(entity.getStatusCode(), equalTo(HttpStatus.OK));

        BigDecimal expectedSaving = getMonthlySaving(monthlyIncome).multiply(BigDecimal.valueOf(12));

        MonthlyStatistic monthlyStatistic = entity.getBody();
        assertThat(monthlyStatistic, notNullValue());
        assertThat(monthlyStatistic.getSavingByYear(), equalTo(expectedSaving));
    }

    @Test
    public void testPerDayAmount () {
        ResponseEntity<MonthlyStatistic> entity = testRestTemplate.getForEntity("/statistic?year={year}&month={month}", MonthlyStatistic.class, now.getYear(), now.getMonthValue());
        assertThat(entity.getStatusCode(), equalTo(HttpStatus.OK));

        MonthlyStatistic monthlyStatistic = entity.getBody();
        assertThat(monthlyStatistic, notNullValue());

        MonthlyIncome backendMonthlyIncome = monthlyStatistic.getMonthlyIncome();
        assertThat(backendMonthlyIncome, notNullValue());
        assertThat(backendMonthlyIncome.getIncomes(), not(empty()));
        assertThat(backendMonthlyIncome.getIncomes(), containsInAnyOrder(this.monthlyIncome.getIncomes().toArray()));

        BigDecimal expectedPerDay = this.monthlyIncome
                .getMonthlyIncome()
                .subtract(getMonthlySaving(this.monthlyIncome))
                .subtract(monthlySpending.getMonthlySpending())
                .divide(BigDecimal.valueOf(now.lengthOfMonth()), 2, RoundingMode.DOWN);

        assertThat(monthlyStatistic.getAmountPerDay(), equalTo(expectedPerDay));
    }

    private BigDecimal getMonthlySaving (MonthlyIncome monthlyIncome) {
        return monthlyIncome
                .getMonthlyIncome()
                .multiply(saving.getPercent())
                .divide(new BigDecimal(100), 2, RoundingMode.HALF_UP);
    }
}
