package application.controller;

import application.entity.Spending;
import application.repository.SpendingRepository;
import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.YearMonth;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class SpendingIntegrationTests extends BaseIntegration {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private SpendingRepository spendingRepository;

    private Spending spending;

    @Before
    public void initSpending () {
        spending = new Spending()
                .setCreationDate(LocalDate.now())
                .setDescription(TEST_DESCRIPTION)
                .setSum(BigDecimal.valueOf(6653, 2));
        spendingRepository.save(spending);
    }

    @After
    public void tearDown () {
        spendingRepository.deleteAll();
    }

    @Test
    public void shouldGetEntityById () {
        ResponseEntity<Spending> entity = restTemplate.getForEntity("/spending/{id}", Spending.class, spending.getId());
        assertThat(entity.getBody(), notNullValue());
        assertThat(entity.getBody(), equalTo(spending));
    }

    @Test
    public void notExistingEntityShouldGet404 () {
        ResponseEntity<Spending> entity = restTemplate.getForEntity("/spending/{id}", Spending.class, 98L);
        assertThat(entity.getStatusCode(), equalTo(HttpStatus.NOT_FOUND));
        assertThat(entity.getBody(), nullValue());
    }


    @Test
    public void shouldCreateSpending () {
        Spending spending = new Spending()
                .setSum(BigDecimal.valueOf(96.45f))
                .setCreationDate(LocalDate.now());

        ResponseEntity<Spending> savedResponse = restTemplate.postForEntity("/spending", spending, Spending.class);
        assertThat(savedResponse.getStatusCode(), equalTo(HttpStatus.CREATED));

        Spending body = savedResponse.getBody();
        assertThat(body, notNullValue());
        assertThat(body.getId(), notNullValue());

        spending.setId(body.getId());
        assertThat(body, equalTo(spending));
    }

    @Test
    public void shouldDeleteSpending () {
        restTemplate.delete("/spending?id={id}", spending.getId());
        Optional<Spending> spendingFromDB = spendingRepository.findById(spending.getId());
        assertThat(spendingFromDB.isPresent(), is(Boolean.FALSE));
    }

    @Test
    public void shouldBeFoundByYearMonth () {
        YearMonth yearMonth = YearMonth.now()
                .minusYears(3)
                .minusMonths(4);

        Spending anotherSpending = new Spending()
                .setSum(BigDecimal.valueOf(34.57).setScale(2))
                .setCreationDate(yearMonth.atDay(5));
        spendingRepository.save(anotherSpending);

        ResponseEntity<List<Spending>> response = restTemplate.exchange("/spending/range?year={year}&month={month}",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<Spending>>() {
                },
                yearMonth.getYear(),
                yearMonth.getMonthValue());
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));

        List<Spending> body = response.getBody();
        assertThat(body, CoreMatchers.allOf(notNullValue(), not(empty())));
        assertThat(body, hasItem(anotherSpending));
        assertThat(body, not(hasItem(spending)));
    }

    @Test
    //TODO: Should be tested with DB tests.
    public void nullDateShouldBeUpdatedToCurrentDate () {
        Spending spending = new Spending()
                .setDescription(TEST_DESCRIPTION)
                .setSum(BigDecimal.valueOf(5656, 2));
        spendingRepository.save(spending);

        ResponseEntity<Spending> entity = restTemplate.getForEntity("/spending/{id}", Spending.class, spending.getId());
        assertThat(entity.getBody(), notNullValue());
        assertThat(entity.getBody(), equalTo(spending));
        assertThat(entity.getBody().getCreationDate(), equalTo(LocalDate.now()));
    }
}
