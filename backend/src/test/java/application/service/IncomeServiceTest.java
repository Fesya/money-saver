package application.service;

import application.entity.Income;
import application.entity.MonthlyIncome;
import application.repository.IncomeRepository;
import com.github.javafaker.Faker;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.time.YearMonth;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class IncomeServiceTest {

    private Faker faker = new Faker();

    @Mock
    private IncomeRepository incomeRepository;

    private IncomeService incomeService;
    private Income income;

    @Before
    public void setUp () {
        income = new Income()
                .setSum(new BigDecimal(57.75));
        incomeService = new IncomeService(incomeRepository);
    }

    @Test
    public void testSaveIncome () {
        when(incomeRepository.save(income)).thenReturn(income);
        Income savedIncome = incomeService.save(income);
        assertThat(savedIncome, equalTo(income));
    }

    @Test
    public void testIncomeByMonth () {
        List<Income> incomes = Arrays.asList(
                new Income().setSum(BigDecimal.valueOf(46.87)),
                new Income().setSum(BigDecimal.valueOf(76.53))
        );

        YearMonth now = YearMonth.now();
        when(incomeRepository.findAllByCreationDateBetween(now.atDay(1), now.atEndOfMonth()))
                .thenReturn(incomes);

        MonthlyIncome monthlyIncome = incomeService.getIncomeByMonth(now);
        assertThat(monthlyIncome.getMonthlyIncome(), equalTo(BigDecimal.valueOf(123.40).setScale(2)));
    }
}
