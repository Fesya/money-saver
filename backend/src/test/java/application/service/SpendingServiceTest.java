package application.service;

import application.entity.MonthlySpending;
import application.entity.Spending;
import application.repository.SpendingRepository;
import com.github.javafaker.Faker;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.YearMonth;
import java.util.Collections;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class SpendingServiceTest {

    private final Faker faker = new Faker();
    private Spending spending;
    @Mock
    private SpendingRepository spendingRepository;

    private SpendingService spendingService;

    @Before
    public void init () {
        spending = new Spending()
                .setId(10L)
                .setSum(new BigDecimal(25.65f))
                .setCreationDate(LocalDate.now())
                .setDescription(faker.book().author());

        spendingService = new SpendingService(spendingRepository);
    }

    @Test
    public void testSaveSpending () {
        when(spendingRepository.save(spending)).thenReturn(spending);
        Spending savedSpending = spendingService.save(spending);
        assertThat(savedSpending, equalTo(spending));
    }

    @Test
    public void testDeleteById () {
        doNothing().when(spendingRepository).deleteById(spending.getId());
        spendingService.deleteById(spending.getId());
        verify(spendingRepository, times(1)).deleteById(spending.getId());
    }

    @Test
    public void testFindById () {
        when(spendingRepository.findById(100L)).thenReturn(Optional.of(spending));
        Optional<Spending> foundSpending = spendingService.findById(100L);
        assertThat(foundSpending.isPresent(), is(Boolean.TRUE));
        assertThat(foundSpending.get(), is(spending));
    }

    @Test
    public void testFindByYearMonth () {
        YearMonth yearMonth = YearMonth.now();
        when(spendingRepository.findAllByCreationDateBetween(yearMonth.atDay(1), yearMonth.atEndOfMonth()))
                .thenReturn(Collections.singletonList(spending));
        MonthlySpending spendings = spendingService.getSpendingByMonth(yearMonth);
        assertThat(spendings, notNullValue());
        assertThat(spendings.getSpendings(), allOf(notNullValue(), hasItem(this.spending)));
        assertThat(spendings.getMonthlySpending(), equalTo(BigDecimal.valueOf(2564, 2)));
    }
}
