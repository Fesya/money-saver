package application.service;

import application.entity.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.time.Month;
import java.time.YearMonth;
import java.util.Arrays;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class StatisticServiceTest {

    @Mock
    private IncomeService incomeService;

    @Mock
    private SavingService savingService;

    @Mock
    private SpendingService spendingService;

    private StatisticService statisticService;
    private YearMonth now;
    private MonthlyStatistic statisticByYearMonth;

    @Before
    public void setUp () {
        now = YearMonth.of(2000, Month.AUGUST);

        MonthlyIncome monthlyIncome = new MonthlyIncome(
                Arrays.asList(
                        new Income().setSum(BigDecimal.valueOf(350.60)),
                        new Income().setSum(BigDecimal.valueOf(270.30))
                )//620.90
        );

        Saving saving = new Saving()
                .setPercent(new BigDecimal(37));//391.17

        MonthlySpending monthlySpending = new MonthlySpending(
                Arrays.asList(
                        new Spending().setSum(BigDecimal.valueOf(150.10)),
                        new Spending().setSum(BigDecimal.valueOf(205.15))
                )//355.25
        );//1.15


        when(incomeService.getIncomeByMonth(now)).thenReturn(monthlyIncome);
        when(spendingService.getSpendingByMonth(now)).thenReturn(monthlySpending);
        when(savingService.getSaving(now)).thenReturn(Optional.of(saving));

        statisticService = new StatisticService(incomeService, savingService, spendingService);
        statisticByYearMonth = statisticService.getStatisticByYearMonth(now);
    }

    @Test
    public void testSavingByMonth () {
        assertThat(statisticByYearMonth.getSavingsByMonth(), equalTo(BigDecimal.valueOf(229.73)));
    }

    @Test
    public void testSavingByYear () {
        assertThat(statisticByYearMonth.getSavingByYear(), equalTo(BigDecimal.valueOf(2756.76)));
    }

    @Test
    public void testPerDayAmount () {
        assertThat(statisticByYearMonth.getAmountPerDay(), equalTo(BigDecimal.valueOf(115, 2)));
    }
}
